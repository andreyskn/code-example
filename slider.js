import $ from 'jquery';
import 'slick-carousel';

export default class Slider {
  static init(context = document) {
    const sliders = [...context.querySelectorAll('.slider')];

    if (sliders.length) {
      sliders.map(el => {
        new Slider(el);
      });
    }
  }

  constructor(slider) {
    this.slider = slider;
    this.sliderContent = $(this.slider.querySelector('.slider__content'));
    this.btnPrev = $(this.slider.querySelector('.slider__btn-prev'));
    this.btnNext = $(this.slider.querySelector('.slider__btn-next'));
    this.counter = $(this.slider.querySelector('.slider__counter'));
    this.progressBar = this.slider.querySelector('.progress-bar');
    this.progressBarItem = this.progressBar.querySelector('.progress-bar__item');
    this.slidesAmount = [...this.slider.querySelectorAll('.slider__item')].length;
    this.slideImages = [...this.slider.querySelectorAll('.slider__image')];
    this.progressBarCopy = null;
    this.barWidth = null;
    this.barPositio = null;
    this.hint = this.slider.querySelector('.slider__hint');

    this.init();
  }

  init() {
    this.sliderContent
      .on('init', () => {
        this.setProgressBarWidth();
        this.adjustSlidePosition();
        this.adjustTextPosition();
        this.showHint();
      }).slick({
        prevArrow: this.btnPrev,
        nextArrow: this.btnNext,
      }).on('beforeChange', (event, slick, currentSlide, nextSlide) => {
        this.showSlideNumber(event, slick, currentSlide, nextSlide);
        this.showSlideProgress(event, slick, currentSlide, nextSlide);
      });

    if (this.slider.classList.contains('slider_theme_top')) {
      window.addEventListener('resize', () => {
        this.adjustTextPosition();
        this.adjustSlidePosition();
        setTimeout(() => {
          this.adjustTextPosition();
          this.adjustSlidePosition();
        }, 50);
      });
    }

  }

  showSlideNumber(event, slick, currentSlide, nextSlide) {
    let formattedNumber = `${nextSlide + 1}`.padStart(2, '0');

    this.counter.html(formattedNumber);
  }

  showSlideProgress(event, slick, currentSlide, nextSlide) {
    if (!nextSlide) {
      const extraBars = [...this.progressBar.querySelectorAll('.progress-bar__item:not(:first-of-type)')];

      extraBars.map(el => el.remove());
      this.progressBarCopy.style.left = `${this.barWidth}%`;
      return;
    }

    if (nextSlide + 1 === this.slidesAmount) {
      const barsAmount = [...this.progressBar.querySelectorAll('.progress-bar__item')].length;
      const barsDiff = this.slidesAmount - barsAmount;
      const arr = new Array(barsDiff);

      for (const i of arr) {
        this.addProgessBar();
      }
      return;
    }

    if (nextSlide > currentSlide) {
      this.addProgessBar();
    } else {
      this.removeProgessBar();
    }
  }

  addProgessBar() {
    this.barPosition = parseFloat(this.progressBarCopy.style.left);
    const newBar = this.progressBarCopy.cloneNode();
    this.progressBar.appendChild(newBar);
    this.progressBarCopy.style.left = `${this.barPosition + this.barWidth}%`;
  }

  removeProgessBar() {
    this.barPosition = parseFloat(this.progressBarCopy.style.left);
    const lastBar = this.progressBar.querySelector('.progress-bar__item:last-of-type');

    lastBar.remove();
    this.progressBarCopy.style.left = `${this.barPosition - this.barWidth}%`;
  }

  setProgressBarWidth() {
    this.barWidth = 100 / this.slidesAmount;

    this.progressBarItem.style.width = `${this.barWidth}%`;
    this.progressBarCopy = this.progressBarItem.cloneNode();
    this.progressBarCopy.style.left = `${this.barWidth}%`;
  }

  adjustSlidePosition() {
    if (this.slider.classList.contains('slider_theme_top')) {
      const slides = [...this.slider.querySelectorAll('.slick-slide')];

      slides.map(el => {
        const distanceToTop = el.offsetTop;

        el.style.transform = `translateY(-${distanceToTop}px)`;
      });
    }
  }

  adjustTextPosition() {
    const isTopSlider = this.slider.classList.contains('slider_theme_top');
    const slidesText = [...this.slider.querySelectorAll('.slider__item-text')];
    const sliderHeight = this.slider.offsetHeight;

    if (isTopSlider) {
      slidesText.map(el => {
        const textHeight = el.offsetHeight;
        let margin;

        if (window.innerWidth > 768) {
          margin = (sliderHeight - textHeight) / 2;
        } else {
          margin = (sliderHeight - textHeight - 40) / 2;
        }

        el.style.marginTop = `${margin}px`;
      });
    } else {
      slidesText.map(el => {
        const textHeight = el.offsetHeight;
        let margin;

        if (window.innerWidth < 768) {
          margin = (sliderHeight - textHeight) / 2;
        }

        el.style.marginTop = `${margin}px`;
      });
    }

  }

  showHint() {
    if (this.hint && window.innerHeight === this.slider.offsetHeight) {
      this.hint.classList.add('slider__hint_active');
    }
  }
}
