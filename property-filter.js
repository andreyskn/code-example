export default class PropFilter {
  static init(context = document) {
    const modal = context.querySelector('.modal_property');

    if (modal) {
      new PropFilter(modal);
    }
  }

  constructor(modal) {
    this.modal = modal;
    this.filter = this.modal.querySelector('.propery-filter');
    this.selects = [...this.filter.querySelectorAll('.select')];
    this.resetBtn = this.filter.querySelector('.propery-filter__resetBtn');
    this.resetCounter = this.resetBtn.querySelector('.button__counter');
    this.emptyCounterClass = 'button__counter_empty';
    this.activeFilterClass = 'propery-filter__filters_active';
    this.showFilterBtn = this.filter.querySelector('.more-filters');
    this.filterBody = this.filter.querySelector('.propery-filter__body');
    this.mainFilters = this.filter.querySelector('.propery-filter__filters_visible');
    this.extraFilters = this.filter.querySelector('.propery-filter__filters_hidden');
    this.extraFiltersShown = false;
    this.closeBtn = this.filter.querySelector('.propery-filter__close');
    this.overlay = this.modal.querySelector('.modal__overlay');
    this.modalOpenBtns = [...document.querySelectorAll('[data-modal="property-filter"]')];
    this.resetChecksBtn = document.querySelector('.popup__resetBtn');
    this.roomPopup = null;
    this.checks = null;
    this.rangePopup = null;
    this.scroller = this.filter.querySelector('.propery-filter__scroller');

    this.init();
  }

  init() {
    this.setUpListeners();
  }

  createRoomPopup() {
    const popup = document.createElement('div');
    popup.classList.add('popup', 'popup_check');
    const checkboxes = document.createElement('div');
    checkboxes.classList.add('popup__checkboxes');
    popup.appendChild(checkboxes);

    const data = [
      { type: '0-room', text: 'Студия' },
      { type: '1-room', text: 'Одна' },
      { type: '2-room', text: 'Две' },
      { type: '3-room', text: 'Три' },
      { type: '4-room', text: 'Четыре и больше' },
      { type: '5-room', text: 'Свободная планировка' }
    ];

    data.map(el => {
      const item = document.createElement('div');
      item.classList.add('popup__item');

      const input = document.createElement('input');
      input.classList.add('popup__check');
      input.type = 'checkbox';
      input.id = el.type;
      item.appendChild(input);

      const mark = document.createElement('div');
      mark.classList.add('popup__checkmark');
      item.appendChild(mark);

      const label = document.createElement('label');
      label.classList.add('popup__label');
      label.setAttribute('for', el.type);
      label.innerText = el.text;
      item.appendChild(label);
      checkboxes.appendChild(item);
    });

    const footer = document.createElement('div');
    footer.classList.add('popup__footer');
    const resetBtn = this.resetChecksBtn.cloneNode(true);
    resetBtn.style.display = 'flex';
    footer.appendChild(resetBtn);
    popup.appendChild(footer);

    this.roomPopup = document.body.appendChild(popup);
    this.handleCheckReset(resetBtn);
  }

  handleCheckReset(button) {
    button.addEventListener('click', () => {
      const select = this.filter.querySelector('.select_active');
      const title = select.querySelector('.select__title');
      const data = select.dataset.active;

      this.checks.map(el => {
        el.checked = false;
      });

      if (data.length > 1) {
        let counter = parseInt(this.resetCounter.innerText, 10);

        this.resetCounter.innerText = `${--counter}`;

        if (counter === 0) {
          this.resetCounter.classList.add(this.emptyCounterClass);
        } else {
          this.resetCounter.classList.remove(this.emptyCounterClass);
        }
      }

      select.dataset.active = 0;
      title.innerText = 'Все';
    });
  }

  createRangePopup(type) {
    const popup = document.createElement('div');
    popup.classList.add(type);

    const from = document.createElement('div');
    from.classList.add(`${type}__from`);

    const fromText = document.createElement('div');
    fromText.classList.add(`${type}__text`);
    fromText.innerText = 'от';
    from.appendChild(fromText);

    const fromInput = document.createElement('input');
    fromInput.classList.add(`${type}__input`);
    fromInput.type = 'text';
    fromInput.placeholder = '___';
    fromInput.inputMode = 'numeric';
    from.appendChild(fromInput);

    const to = document.createElement('div');
    to.classList.add(`${type}__to`);

    const toText = document.createElement('div');
    toText.classList.add(`${type}__text`);
    toText.innerText = 'до';
    to.appendChild(toText);

    const toInput = document.createElement('input');
    toInput.classList.add(`${type}__input`);
    toInput.type = 'text';
    toInput.placeholder = '___';
    toInput.inputMode = 'numeric';
    to.appendChild(toInput);

    const units = document.createElement('div');
    units.classList.add(`${type}__units`);

    if (type === 'range-area') {
      const meter = document.createElement('div');
      meter.classList.add(`${type}__meter`);
      meter.innerHTML = 'м<sup>2</sup>';
      units.appendChild(meter);
    } else {
      const rub = document.createElement('div');
      rub.classList.add(`${type}__rub`);
      rub.innerHTML = '&#8381;';
      units.appendChild(rub);
    }

    popup.appendChild(from);
    popup.appendChild(to);
    popup.appendChild(units);

    this.rangePopup = document.body.appendChild(popup);
  }

  showOptions(node) {
    const left = node.getBoundingClientRect().left;
    const right = node.getBoundingClientRect().right;
    const bottom = node.getBoundingClientRect().bottom;
    const data = JSON.parse(node.dataset.options);
    const activeId = parseInt(node.dataset.active, 10);
    const isGroup = node.dataset.group;
    const type = data[1];
    const [, ...activeArr] = [...node.dataset.active];

    switch (type) {
      case 'checks':
        this.createRoomPopup();
        this.roomPopup.style.left = `${left}px`;
        this.roomPopup.style.top = `${bottom + 6}px`;
        this.roomPopup.classList.add('active-popup');
        this.checks = [...this.roomPopup.querySelectorAll('input')];
        activeArr.map(index => {
          this.checks[index].checked = true;
        });
        this.handleChecks(node);
        break;

      case 'range1':
        this.createRangePopup('range-price');
        if (window.innerWidth > 450) {
          this.rangePopup.style.left = 'unset';
          this.rangePopup.style.right = `${window.innerWidth - right - 18}px`;
        } else {
          this.rangePopup.style.left = `${left}px`;
        }
        this.rangePopup.style.top = `${bottom + 6}px`;
        this.rangePopup.classList.add('active-popup');
        this.handleInput(node);
        break;

      case 'range2':
        this.createRangePopup('range-area');
        this.rangePopup.style.left = `${left}px`;
        this.rangePopup.style.top = `${bottom + 6}px`;
        this.rangePopup.classList.add('active-popup');
        this.handleInput(node);
        break;

      default: {
        const opts = document.createElement('div');
        opts.classList.add('select__options');
        if (isGroup) {
          opts.classList.add('select__options_wide');
        }

        data.map((el, index) => {
          const opt = document.createElement('div');

          if (typeof el === 'string') {
            opt.classList.add('select__item');
            if (index === activeId) {
              opt.classList.add('select__item_active');
            }
            opt.innerText = el;
          } else {
            opt.classList.add('select__group');
            opt.innerText = el.group;
          }

          opts.appendChild(opt);
        });

        const appended = document.body.appendChild(opts);
        appended.style.left = `${left}px`;
        appended.style.top = `${bottom + 6}px`;
        this.listenItemClicks(appended, node);
        break;
      }
    }
  }

  handleInput(caller) {
    const inputs = [...this.rangePopup.querySelectorAll('input')];
    let data = caller.dataset.active;
    let parsedData = JSON.parse(data);

    if (typeof parsedData === 'object') {
      Object.keys(parsedData).map(key => {
        inputs[+key].value = parsedData[key];
      });
    }

    inputs.map((input, index) => {
      input.addEventListener('input', () => {
        const value = input.value;
        const formatted = [...value].filter(char => char.match(/\d/)).join('');
        input.value = formatted;

        data = caller.dataset.active;
        parsedData = JSON.parse(data);
        const text = input.value;
        const titleNode = caller.querySelector('.select__title');

        if (text !== '') {
          if (typeof parsedData === 'number') {
            caller.dataset.active = `{"${index}":"${text}"}`;
          } else {
            parsedData[index] = text;
            caller.dataset.active = JSON.stringify(parsedData);
            // titleNode.innerText = index ? `До ${text}` : `От ${text}$nbsp;`;
          }
        } else {
          if (Object.keys(parsedData).length === 1) {
            caller.dataset.active = 0;
          } else {
            delete parsedData[index];
            caller.dataset.active = JSON.stringify(parsedData);
          }
        }

        if (typeof parsedData === 'object') {
          if (Object.keys(parsedData).length === 1) {
            const key = Object.keys(parsedData)[0];

            titleNode.innerText = +key ? `до ${parsedData[key]}` : `от ${parsedData[key]} `;
          } else {
            titleNode.innerText = `от ${parsedData[0]} до ${parsedData[1]}`;
          }
        } else {
          titleNode.innerText = index ? `до ${text}` : `от ${text} `;
        }

        if (caller.dataset.active === '0') {
          titleNode.innerText = 'Любая';
        }

        let counter = parseInt(this.resetCounter.innerText, 10);

        if (typeof parsedData === 'number' && text !== '') {
          this.resetCounter.innerText = `${++counter}`;
        }

        if (typeof parsedData === 'object' && text === '' && Object.keys(JSON.parse(data)).length === 1) {
          this.resetCounter.innerText = `${--counter}`;
        }

        if (counter === 0) {
          this.resetCounter.classList.add(this.emptyCounterClass);
        } else {
          this.resetCounter.classList.remove(this.emptyCounterClass);
        }
      });
    });
  }

  handleChecks(caller) {
    this.checks.map((check, index) => {
      check.addEventListener('change', () => {
        const data = caller.dataset.active;
        const text = check.parentNode.querySelector('.popup__label').innerText;
        const titleNode = caller.querySelector('.select__title');
        const titleText = titleNode.innerText;
        let counter = parseInt(this.resetCounter.innerText, 10);

        if (check.checked) {
          caller.dataset.active += index;

          if (caller.dataset.active.length === 2) {
            this.resetCounter.innerText = `${++counter}`;
          }

          if (titleText === 'Все') {
            titleNode.innerText = text;
          } else {
            titleNode.innerText = 'Несколько';
          }

        } else {
          const [, ...actives] = [...data];
          const filtered = actives.reduce((arr, cur) => {
            if (+cur !== index) {
              arr.push(cur);
            }
            return arr;
          }, []);
          caller.dataset.active = `0${filtered.join('')}`;

          const active = caller.dataset.active;

          if (active.length === 1) {
            this.resetCounter.innerText = `${--counter}`;
            titleNode.innerText = 'Все';
          }
          if (active.length === 2) {
            titleNode.innerText = this.checks[active[1]].parentNode.querySelector('.popup__label').innerText;
          }
        }

        if (counter === 0) {
          this.resetCounter.classList.add(this.emptyCounterClass);
        } else {
          this.resetCounter.classList.remove(this.emptyCounterClass);
        }
      });
    });
  }

  listenItemClicks(node, caller) {
    const items = [...node.children];
    const title = caller.querySelector('.select__title');

    items.map((el, index) => {
      el.addEventListener('click', (e) => {
        const _this = e.currentTarget;
        const active = caller.dataset.active;

        if (!_this.classList.contains('select__item_active')) {
          items.map(item => item.classList.remove('select__item_active'));

          _this.classList.add('select__item_active');
          title.innerText = _this.innerText;
          caller.dataset.active = index;
          this.hideOptions();
          caller.classList.remove('select_active');

          let counter = parseInt(this.resetCounter.innerText, 10);

          if (index > 0 && active === '0') {
            this.resetCounter.innerText = `${++counter}`;
          }
          if (index === 0 && active !== '0') {
            this.resetCounter.innerText = `${--counter}`;
          }

          if (counter === 0) {
            this.resetCounter.classList.add(this.emptyCounterClass);
          } else {
            this.resetCounter.classList.remove(this.emptyCounterClass);
          }

        } else {
          this.hideOptions();
          caller.classList.remove('select_active');
        }
      });
    });
  }

  hideOptions() {
    const options = document.querySelector('.select__options');

    if (options) {
      options.remove();
    }
    if (this.roomPopup) {
      this.roomPopup.remove();
    }
    if (this.rangePopup) {
      this.rangePopup.remove();
    }
  }

  filterReset() {
    this.selects.map(select => {
      const titleText = JSON.parse(select.dataset.options)[0];
      const titleNode = select.querySelector('.select__title');

      titleNode.innerText = titleText;
      select.dataset.active = 0;
    });
  }

  setUpListeners() {
    this.modal.addEventListener('click', (e) => {
      const target = e.target;

      if (target.getAttribute('class') && !target.getAttribute('class').match(/(select|label)/g)) {
        this.hideOptions();
        this.selects.map(el => el.classList.remove('select_active'));
      }
    });

    this.selects.map(select => {
      select.addEventListener('click', (e) => {
        const self = e.currentTarget;
        this.hideOptions();

        if (!self.classList.contains('select_active')) {
          this.selects.map(el => el.classList.remove('select_active'));
          self.classList.add('select_active');
          this.showOptions(self);
        } else {
          self.classList.remove('select_active');
        }

      });
    });

    this.resetBtn.addEventListener('click', e => {
      e.preventDefault();

      const counter = parseInt(this.resetCounter.innerText, 10);

      if (counter > 0) {
        this.filterReset();
        this.resetCounter.innerText = '0';
        this.resetCounter.classList.add(this.emptyCounterClass);
      }
    });

    this.showFilterBtn.addEventListener('click', e => {
      e.preventDefault();

      const textNode = this.showFilterBtn.querySelector('.button__text');

      this.extraFiltersShown = !this.extraFiltersShown;
      this.extraFilters.classList.toggle(this.activeFilterClass);
      this.showFilterBtn.classList.toggle('less');

      if (this.extraFiltersShown) {
        textNode.innerText = 'Меньше деталей';
      } else {
        textNode.innerText = 'Еще детали';
      }
    });

    [this.closeBtn, this.overlay].map(el => {
      el.addEventListener('click', e => {
        e.preventDefault();

        this.modal.classList.remove('modal_active');
        this.modal.classList.add('modal_hidden');
        this.hideOptions();
        this.selects.map(sel => sel.classList.remove('select_active'));
      });
    });

    this.modalOpenBtns.map(el => {
      el.addEventListener('click', e => {
        e.preventDefault();

        this.modal.classList.add('modal_active');
      });
    });

    window.addEventListener('resize', () => {
      const select = this.filter.querySelector('.select_active');
      const options = document.querySelector('.select__options');
      const popup = document.querySelector('.active-popup');

      if (select) {
        const left = select.getBoundingClientRect().left;
        const bottom = select.getBoundingClientRect().bottom;

        if (options) {
          options.style.left = `${left}px`;
          options.style.top = `${bottom + 6}px`;
        }

        if (popup) {
          if (popup.classList.contains('range-price') && window.innerWidth > 450) {
            const right = select.getBoundingClientRect().right;

            popup.style.left = 'unset';
            popup.style.right = `${window.innerWidth - right - 18}px`;
            popup.style.top = `${bottom + 6}px`;
          } else {
            popup.style.left = `${left}px`;
            popup.style.top = `${bottom + 6}px`;
          }
        }
      }
    });

    this.scroller.addEventListener('scroll', () => {
      const select = this.filter.querySelector('.select_active');
      const options = document.querySelector('.select__options');
      const popup = document.querySelector('.active-popup');

      if (select) {
        const left = select.getBoundingClientRect().left;
        const bottom = select.getBoundingClientRect().bottom;

        if (options) {
          options.style.left = `${left}px`;
          options.style.top = `${bottom + 6}px`;
        }

        if (popup) {
          popup.style.left = `${left}px`;
          popup.style.top = `${bottom + 6}px`;
        }

      }
    });
  }

}
