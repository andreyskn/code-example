export default class CityFilter {
  static init(context = document) {
    const modal = context.querySelector('.modal_city');

    if (modal) {
      new CityFilter(modal);
    }
  }

  constructor(modal) {
    this.modal = modal;
    this.filter = this.modal.querySelector('.city-filter');
    this.inputField = this.filter.querySelector('.city-filter__input');
    this.regionContent = this.filter.querySelector('.city-filter__content_region');
    this.regions = [...this.regionContent.querySelectorAll('.city-filter__item')];
    this.cityContent = this.filter.querySelector('.city-filter__content_city');
    this.cities = [...this.cityContent.querySelectorAll('.city-filter__item')];
    this.inputHolder = this.filter.querySelector('.city-filter__input-holder');
    this.saveBtn = this.filter.querySelector('.save-city');
    this.disabledClass = 'button_disabled';
    this.closeBtn = this.filter.querySelector('.city-filter__close');
    this.overlay = this.modal.querySelector('.modal__overlay');
    this.modalActiveClass = 'modal_active';
    this.modalOpenBtns = [...document.querySelectorAll('[data-modal="city-filter"]')];
    this.resetBtn = this.filter.querySelector('.city-filter__reset');
    this.activeResetClass = 'city-filter__reset_active';
    this.contentBlocks = [...this.filter.querySelectorAll('.city-filter__content-block')];

    this.init();
  }

  init() {
    this.setUpListeners();
  }

  setUpListeners() {
    this.inputField.addEventListener('input', () => {
      const value = this.inputField.value;

      this.search(value);
    });

    this.regions.map(el => {
      el.addEventListener('click', e => {
        e.preventDefault();

        const target = e.currentTarget;
        const value = target.innerText;

        this.resetBtn.classList.add(this.activeResetClass);
        this.inputHolder.innerText = `${value}, `;
        this.inputHolder.style.display = 'block';
        this.inputField.value = '';
        this.inputField.placeholder = 'Выберите город';
        this.inputField.focus();
        this.showCities();
      });
    });

    this.resetBtn.addEventListener('click', () => {
      this.resetForm();
    });

    this.cities.map(el => {
      el.addEventListener('click', e => {
        e.preventDefault();

        const target = e.currentTarget;
        const value = target.innerText;

        this.inputField.value = value;
        this.saveBtn.classList.remove(this.disabledClass);
      });
    });

    [this.saveBtn, this.closeBtn, this.overlay].map(el => {
      el.addEventListener('click', e => {
        e.preventDefault();

        this.resetForm();
        this.modal.classList.remove(this.modalActiveClass);
        this.modal.classList.add('modal_hidden');
      });
    });

    this.modalOpenBtns.map(el => {
      el.addEventListener('click', e => {
        e.preventDefault();

        this.modal.classList.add(this.modalActiveClass);
      });
    });
  }

  resetForm() {
    this.resetBtn.classList.remove(this.activeResetClass);
    this.inputHolder.style.display = 'none';
    this.inputField.value = '';
    this.regionContent.classList.add('active');
    this.cityContent.classList.remove('active');
    this.contentBlocks.map(el => {
      el.removeAttribute('style');
    });
    [...this.regions, ...this.cities].map(el => {
      el.removeAttribute('style');
      el.removeAttribute('data-skip');
      el.removeAttribute('data-keep');
    });
  }

  showCities() {
    [this.regionContent, this.cityContent].map(el => {
      el.classList.toggle('active');
    });
  }

  search(searchInput) {
    const val = searchInput.toLowerCase();
    const searchContext = this.filter.querySelector('.city-filter__content.active');
    const blocks = [...searchContext.querySelectorAll('.city-filter__content-block')];
    const items = [...searchContext.querySelectorAll('.city-filter__item')];

    items.map(el => {
      el.removeAttribute('data-keep');
      el.removeAttribute('data-skip');
      el.removeAttribute('style');
    });

    blocks.map(el => {
      el.removeAttribute('style');
    });

    items.map(el => {
      const text = el.innerText.toLowerCase();

      if (text.includes(val)) {
        el.dataset.keep = 'true';
      } else {
        el.dataset.skip = 'true';
      }
    });

    items.map(el => {
      if (!el.dataset.keep) {
        el.style.display = 'none';
      }
    });

    blocks.map(el => {
      const children = el.children;
      const childrenAmount = children.length - 1;
      let counter = 0;

      [...children].map(item => {
        if (item.dataset.skip) {
          counter++;
        }
      });

      if (counter === childrenAmount) {
        el.style.display = 'none';
      }
    });
  }
}
